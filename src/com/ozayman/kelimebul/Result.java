package com.ozayman.kelimebul;

public class Result {
	String kelime;
	String path;
	
	public Result(String kelime, String path) {
		this.kelime = kelime;
		this.path = path;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Result) {
			Result r = (Result) obj;
			return r.kelime.equals(kelime);
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		return kelime.hashCode();
	}
	
	

}
