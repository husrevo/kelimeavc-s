package com.ozayman.kelimebul;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.android.chimpchat.adb.AdbBackend;
import com.android.chimpchat.adb.AdbChimpDevice;
import com.android.chimpchat.core.IChimpImage;

public class Device {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static char[][] getMatrix(AdbChimpDevice device) throws IOException {
		
		char[][] result = new char[4][4];
		
		IChimpImage img = device.takeSnapshot();

		int startX = 64;
		int startY = 472;
		int padding = 12;
		
		String fileName = "/home/husrev/workspaces/java/KelimeBul/pngs/";
		String fileExtension = ".png";
		
	
		BufferedImage[] charImgs = new BufferedImage[29];
		int k =0;
		for(char c: WordElement.chars.toCharArray()) {
			charImgs[k++] =  ImageIO.read(new File(fileName+c+fileExtension));
		}
		
		int maxX = charImgs[0].getWidth();
		int maxY = charImgs[0].getHeight();

		

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				
				BufferedImage bim = img.getSubImage(startX + padding + j * 154,
						startY + padding + i * 154, 132 - 2 * padding,
						132 - 2 * padding).getBufferedImage();
				boolean found = false;
				int c = 0;
				
				while(found == false && c<29) {
					int matching = 0;
					int mismatch = 0;
					for(int x = 0; x< maxX; x+=3) {
						for(int y =0;y<maxY; y+=3) {
							int rgb = bim.getRGB(x, y);
							int r = 0xFF - (rgb & 0xFF0000) / 0x10000;
							int g = 0xFF - (rgb & 0x00FF00) / 0x100;
							int b = 0xFF - rgb & 0X0000FF;
							
							boolean black =r+g+b>0x1FF;
							
							int rgb2 = charImgs[c].getRGB(x, y);
							int r2 = 0xFF -  (rgb2 & 0xFF0000) / 0x10000;
							int g2 = 0xFF -  (rgb2 & 0x00FF00) / 0x100;
							int b2 = 0xFF -  rgb2 & 0X0000FF;
							
							boolean black2 =r2+g2+b2>0x1FF;
							
							
							if(black && black2)
								matching++;
							else if(black || black2)
								mismatch++;
						}
						
					}

					if(matching > mismatch*100){
						result[i][j] = WordElement.chars.charAt(c);
						found = true;
					}
					
					c++;
				}
				
			}
		}
		for(int i =0; i<4; i++)
			System.out.println(""+(char)result[i][0]+(char)result[i][1]+(char)result[i][2]+(char)result[i][3]);
		return result;
	}
}
