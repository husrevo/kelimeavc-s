package com.ozayman.kelimebul;

import java.awt.image.BufferedImage;
import java.awt.image.ImageFilter;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ProcessImages {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		String fileName = "/home/husrev/workspaces/java/KelimeBul/pngs/a.png";
		BufferedImage img = ImageIO.read(new File(fileName));
		
		int maxX = img.getWidth();
		int maxY = img.getHeight();
		for(int x = 0; x< maxX; x ++) {
			for(int y =0;y<maxY; y++) {
				int rgb = img.getRGB(x, y);
				int r = 0xFF - (rgb & 0xFF0000) / 0x10000;
				int g = 0xFF - (rgb & 0x00FF00) / 0x100;
				int b = 0XFF - rgb & 0X0000FF;
				if(r+g+b>0x1FF)
					rgb = 0xFF000000;
				else
					rgb = 0xFFFFFFFF;
				img.setRGB(x, y, rgb);
			}
		}
		
		ImageIO.write(img, "PNG", new File("A.png"));
		

	}

}
