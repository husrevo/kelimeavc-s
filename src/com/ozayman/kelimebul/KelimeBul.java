package com.ozayman.kelimebul;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.Semaphore;

public class KelimeBul {

	private static final int firstRow = 540;
	private static final int firstColumn = 130;
	private static final int perCell = 160;

	private static int getYForRow(int row) {
		return firstRow + row * perCell;
	}

	private static int getXForCol(int col) {
		return firstColumn + col * perCell;
	}

	public static void main(String[] args) throws Exception {

		final MonkeyDragger dragger = new MonkeyDragger();
		final Semaphore semaphore = new Semaphore(0);
		new Thread(new Runnable() {

			@Override
			public void run() {
				dragger.init();
				semaphore.release();
			}
		}).start();

		String[] kelimeler = readLines("/home/husrev/Desktop/kelimeler.txt");

		// System.out.println("dosya okundu");

		for (String kelime : kelimeler) {
			WordElement.rootElement.addWord(kelime);
		}

		System.out.println("ağaç oluşturuldu");
		System.out.println("cihaz bekleniyor.");
		semaphore.acquire();
		
		boolean b = false;
		do {
			char[][] matrix = Device.getMatrix(dragger.getDevice());
			Thread.sleep(500);
			printAllPossibilities(matrix, dragger);
			
			System.out.println("bitti. tekrar devam etmek için devam yazıp enter'a basın, çıkmak için quit yazın.");
			String userInput;
			Scanner scan = new Scanner (System.in);
			userInput = scan.next();
			
			if(userInput.toLowerCase().startsWith("q"))
				b=true;
		} while (!b);

		dragger.backend.shutdown();

	}

	public static String[] readLines(String filename) throws IOException {
		FileReader fileReader = new FileReader(filename);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		List<String> lines = new ArrayList<String>();
		String line = null;

		String regex = "[" + WordElement.chars + "]*";

		while ((line = bufferedReader.readLine()) != null) {
			if (line.matches(regex) && line.length() > 2)
				lines.add(line);
		}
		bufferedReader.close();
		return lines.toArray(new String[lines.size()]);
	}

	private static List<Result> printAllPossibilities(char[][] matrix,
			MonkeyDragger dragger) throws Exception {
		List<Result> results = new ArrayList<Result>();

		int rows = 4;
		int cols = 4;

		boolean[][] used = new boolean[rows][cols];

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				printAllPossibilities(i, j, used, "", "",
						WordElement.rootElement, matrix, rows, cols, results);
			}
		}

		Set<Result> set = new HashSet<Result>(results);
		List<Result> uniqueWords = new ArrayList<Result>(set);

		Collections.sort(uniqueWords, new Comparator<Result>() {

			@Override
			public int compare(Result r1, Result r2) {
				String o1 = r1.kelime;
				String o2 = r2.kelime;

				if (o1.length() != o2.length())
					return (o2.length() - o1.length());
				else
					return o2.compareTo(o1);
			}
		});

		for (Result r : uniqueWords) {

			System.out.println(r.kelime + " yazdırılıyor. " + r.path);
			String[] parts = r.path.split(" ");
			ArrayList<Point> points = new ArrayList<Point>(parts.length);

			for (int i = 0; i < parts.length; i++) {
				String part = parts[i];
				String[] coordinates = part.split(",");
				int x = Integer.parseInt(coordinates[0]);
				int y = Integer.parseInt(coordinates[1]);
				points.add(new Point(x, y));
			}
			dragger.drag(points);
			Thread.sleep(700);
		}

		return results;

	}

	private static void printAllPossibilities(int i, int j, boolean[][] used,
			String path, String touchPath, WordElement previousWordElement,
			char[][] matrix, int rows, int cols, List<Result> out) {
		used[i][j] = true;
		char current = matrix[i][j];

		WordElement currentElement = previousWordElement
				.getNextElementForChar(current);
		if (currentElement == null) {
			used[i][j] = false;
			return;
		}

		path = path + current;
		touchPath = touchPath + getXForCol(j) + "," + getYForRow(i) + " ";
		if (currentElement.isValidWord())
			out.add(new Result(path, touchPath));

		if (currentElement.isHasChild()) {
			for (int rr = -1; rr < 2; rr++)
				for (int cc = -1; cc < 2; cc++) {
					int r = rr + i;
					int c = cc + j;
					if (r >= 0 && r < rows && c >= 0 && c < cols && !used[r][c])
						printAllPossibilities(r, c, used, path, touchPath,
								currentElement, matrix, rows, cols, out);
				}
		}

		//
		// if(i>0 && j>0 && used[i-1][j-1] != true)
		// printAllPossibilities(i-1,
		// j-1,used,path,currentElement,matrix,rows,cols);
		//
		// if(i>0 && j<cols-1 && used[i-1][j+1] != true)
		// printAllPossibilities(i-1, j+1, used, path,currentElement, matrix,
		// rows, cols);
		//
		// if(i<rows-1 && j>0 && used[i+1][j-1] != true)
		// printAllPossibilities(i+1, j-1, used, path,currentElement, matrix,
		// rows, cols);
		//
		// if(i<rows-1 && j<cols-1 && used[i+1][j+1] != true)
		// printAllPossibilities(i+1, j+1, used, path,currentElement, matrix,
		// rows, cols);
		//
		// if(i<rows-1 && used[i+1][j] != true)
		// printAllPossibilities(i+1, j, used, path,currentElement, matrix,
		// rows, cols);
		//
		// if(i>0 && used[i-1][j] != true)
		// printAllPossibilities(i-1, j, used, path,currentElement, matrix,
		// rows, cols);
		//
		// if(j<cols-1 && used[i][j+1] != true)
		// printAllPossibilities(i, j+1, used, path,currentElement, matrix,
		// rows, cols);
		//
		// if(j>0 && used[i][j-1] != true)
		// printAllPossibilities(i, j-1, used, path,currentElement, matrix,
		// rows, cols);

		used[i][j] = false;
	}

}
