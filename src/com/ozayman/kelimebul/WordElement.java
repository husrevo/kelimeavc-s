package com.ozayman.kelimebul;

public class WordElement {
	private String path;
	private boolean validWord;
	private boolean hasChild;
	private WordElement[] nextElements;
	
	public WordElement(String path) {
		this.path = path;
		this.validWord = false;
		this.hasChild = false;
	}
	
	public void addWord(String remainingPartOfTheWord) {
		if(remainingPartOfTheWord.length() == 0 || remainingPartOfTheWord.charAt(0) ==' ')
			validWord = true;
		else {
			if(!hasChild) {
				hasChild = true;
				nextElements = new WordElement[chars.length()];
			}
			
			
			char nextChar = remainingPartOfTheWord.charAt(0);
			WordElement nextElement = nextElements[intsOfChars[nextChar]];
			
			if(nextElement == null) {
				nextElement = new WordElement(path+nextChar);
				nextElements[intsOfChars[nextChar]] = nextElement;
			}
			
			nextElement.addWord(remainingPartOfTheWord.substring(1));
		}
		
	}
	
	public static String chars = "abcçdefgğhıijlkmnoöprsştuüvyz";
	private static char[] getCharsOfNumbers() {
		int size = chars.length();
		char charArray[] = new char[size];
		
		for(int i = 0; i< size; i++)
			charArray[i]=chars.charAt(i);
		
		return charArray;
	}
	
	private static int[] getNumbersOfChars() {
		int size = chars.length();
		int intArray[] = new int[512];
		
		for(int i = 0; i< size; i++)
			intArray[chars.charAt(i)]=i;
		
		return intArray;
	}
	
	private static char[] charsOfNumbers = getCharsOfNumbers();
	private static int[] intsOfChars = getNumbersOfChars();
	
	public static WordElement rootElement = new WordElement("");
	
	public WordElement getNextElementForChar(char c) {		
		return nextElements[intsOfChars[c]];
	}
	
	public boolean isHasChild() {
		return hasChild;
	}
	
	public boolean isValidWord() {
		return validWord;
	}
}
