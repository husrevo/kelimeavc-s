package com.ozayman.kelimebul;

import java.util.List;

import com.android.chimpchat.adb.AdbBackend;
import com.android.chimpchat.adb.AdbChimpDevice;

public class MonkeyDragger {
	AdbBackend backend;
	AdbChimpDevice device;

	public MonkeyDragger() {
		this.backend = new AdbBackend();
	}

	public void init() {
		device = (AdbChimpDevice) backend.waitForConnection();
		
	}
	
	public AdbChimpDevice getDevice() {
		return device;
	}
	
	public void drag(List<Point> points) throws Exception {
		
		Point point = points.get(0);
		device.getManager().touchDown(point.x, point.y);
		Thread.sleep(10);
		int size = points.size();
		for(int i = 1; i<size; i++) {
			point = points.get(i);
			Point prevPoint = points.get(i-1);
			int x = point.x;
			int y = point.y;
			int xPrev = prevPoint.x;
			int yPrev = prevPoint.y;
			for(int step = 0; step < 10; step++){
				int xNew = Math.round(xPrev+(x-xPrev)*step/10f);
				int yNew = Math.round(yPrev+(y-yPrev)*step/10f);
				device.getManager().touchMove(xNew, yNew);
				Thread.sleep(9);
			}
		}
		Point last = points.get(size-1);
		device.getManager().touchUp(last.x, last.y);
		Thread.sleep(100);

	}
	
	

}
