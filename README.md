This is a project that started as a helper ended as an auto-player for a game called 'Kelime Avı'. A literal translation of it to English can be 'Word Hunt'. In a 4x4 grid of chars, you try to find words from neighboring letters. Name of my project means 'Word Hunter'.

Since the code meant to be run on my laptop to manage my phone, most of values are hard-coded, like pixels on screen, path of the dictionary.

The code first creates a Trie using Turkish words from a txt file. Then using an Android testing library, gets a screenshot from the device, reads the table by comparing cells to known values (previously saved as PNGs), then scans for words, sorts them so that the longest one is on top and then, using that test library, it produces drags on the phone. 

Here's a video of the project playing the game:
[https://www.youtube.com/watch?v=u2giQy_H60Y](https://www.youtube.com/watch?v=u2giQy_H60Y)
Please note that the version of the program performing in this video was an earlier version, so I was manually typing the table to the controller computer. The video is not a screen recording, it's another phone camera filming it from the device :) 

Please note that this piece of code doesn't follow any best practices whatsoever, because it's not in production or meant to be maintained :) If you want to see a project where I tried to follow best practices, please see the hiring-challenge-scala-contacts repo.